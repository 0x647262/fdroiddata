OsmAnd+ (OSM Automated Navigation Directions) este o aplicație de hărți și navigație cu acces la datele gratuite, mondiale și de înaltă calitate ale OpenStreetMap (OSM). Bucurați-vă de navigație vocală și optică, de vizualizarea POI (puncte de interes), de crearea și gestionarea de trasee GPX, de utilizarea vizualizării curbelor de nivel și a informațiilor despre altitudine, de posibilitatea de a alege între modurile de condus, ciclism, pietonal, editare OSM și multe altele.

OsmAnd+ este versiunea completă a acestei aplicații. Vă rugăm să donați pentru a susține proiectul, pentru a finanța dezvoltarea de noi funcții și pentru a primi cele mai recente actualizări.

Câteva dintre principalele caracteristici:
Navigație
 • Funcționează online (rapid) sau offline (fără taxe de roaming atunci când vă aflați în străinătate)
 • Ghidare vocală pas cu pas (voci înregistrate și sintetizate)
 • Ghidare opțională pe bandă, afișarea numelui străzii și ora estimată de sosire
 • Suportă punctele intermediare de pe itinerar
 • Re-rutare automată ori de câte ori vă abateți de la traseu
 • Căutare de locuri după adresă, după tip (de exemplu: restaurant, hotel, benzinărie, muzeu) sau după coordonate geografice

Vizualizarea hărții
 • Afișați poziția și orientarea dvs.
 • Aliniați opțional imaginea în funcție de busolă sau de direcția dvs. de deplasare
 • Salvați cele mai importante locuri ca favorite
 • Afișați POI (puncte de interes) în jurul dvs.
 • Afișați dale online specializate, vedere prin satelit (de la Bing), diferite suprapuneri, cum ar fi trasee GPX de turism/navigație și straturi suplimentare cu transparență personalizabilă
 • Afișați opțional nume de locuri în engleză, local sau ortografie fonetică

Utilizați datele OSM și Wikipedia
 • Informații de înaltă calitate de la cele mai bune proiecte de colaborare din lume
 • Date OSM disponibile pe țară sau regiune
 • POI-uri Wikipedia, excelente pentru vizitarea obiectivelor turistice
 • Descărcări gratuite nelimitate, direct din aplicație
 • Hărți vectoriale offline compacte, actualizate cel puțin o dată pe lună
 
 • Selecție între datele complete ale regiunii și doar rețeaua rutieră (Exemplu: Toată Japonia este 700 MB sau 200 MB doar pentru rețeaua rutieră)

Funcții de siguranță
 • Comutare automată opțională a vederii pe timp de zi/noapte
 • Afișare opțională a limitei de viteză, cu memento dacă o depășiți
 • Zoom opțional în funcție de viteză
 • Împărtășiți locația dvs. pentru ca prietenii dvs. să vă poată găsi

Caracteristici pentru bicicliști și pietoni
 • Vizualizarea căilor de mers pe jos, drumeții și piste de biciclete, excelente pentru activități în aer liber
 • Moduri speciale de rutare și afișare pentru biciclete și pietoni
 • Opriri opționale de transport public (autobuz, tramvai, tren), inclusiv nume de linii
 • Înregistrare opțională a călătoriei în fișierul GPX local sau în serviciul online
 • Afișare opțională a vitezei și altitudinii
 • Afișare a curbelor de nivel și a umbririi dealurilor (prin intermediul unui plugin suplimentar)

Anti-funcții:
* NonFreeAssets - Lucrările artistice și machetele sunt sub o licență necomercială.
